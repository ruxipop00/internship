package com.tora;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringJoiner {
    private String delimiter;
    private String prefix;
    private String suffix;
    private List<String> tokens;


    public StringJoiner(CharSequence delimiter) {
        this(delimiter, "", "");
    }

    public StringJoiner(CharSequence delimiter, CharSequence prefix, CharSequence suffix) {
        this.delimiter = delimiter.toString();
        this.prefix = prefix.toString();
        this.suffix = suffix.toString();
        this.tokens = new ArrayList<>();
    }

    public String buildString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (prefix != null) {
            stringBuilder.append(prefix);
        }

        stringBuilder.append(tokens.get(0));

        for(int i = 1; i< tokens.size(); i++) {
            stringBuilder.append(delimiter);
            stringBuilder.append(tokens.get(i));
        }

        if (suffix != null) {
            stringBuilder.append(suffix);
        }

        return stringBuilder.toString();
    }

    public String getString() {
        return buildString();
    }

    public void setDelimiter(CharSequence delimiter) {
        this.delimiter = delimiter.toString();
    }

    public StringJoiner addToken(CharSequence elem) {
        String token = elem.toString();
        tokens.add(token);

        return this;
    }

    public StringJoiner removeToken(CharSequence elem) {
        String token = elem.toString();
        tokens.removeAll(Collections.singleton(token));

        return this;
    }
}
