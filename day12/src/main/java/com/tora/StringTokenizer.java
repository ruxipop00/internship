package com.tora;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class StringTokenizer {
    private String string;
    private String delimiter;

    public StringTokenizer(String string) {
        this(string, " ");
    }

    public StringTokenizer(String string, String delimiter) {
        this.string = string;
        this.delimiter = delimiter;
    }

    public void setString(String string) {
        this.string = string;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    public List<String> getTokens() {
        return Arrays.asList(string.split(Pattern.quote(delimiter)));
    }

    public String getTokenAt(int index) {
        if (index < 0 || index >= countTokens()) {
            throw new IllegalArgumentException("Index out of bounds!");
        }

        List<String> tokens = Arrays.asList(string.split(Pattern.quote(delimiter)));
        return tokens.get(index);
    }

    public int countTokens() {
        List<String> tokens = Arrays.asList(string.split(Pattern.quote(delimiter)));

        return tokens.size();
    }
}
