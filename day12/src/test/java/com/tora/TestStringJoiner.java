package com.tora;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestStringJoiner {
    private StringJoiner stringJoiner;
    private String delimiter;

    @Before
    public void setup() {
        delimiter = ", ";
        stringJoiner = new StringJoiner(delimiter);
    }

    @Test
    public void testAddToken() {
        stringJoiner.addToken("let");
        stringJoiner.addToken("me");
        stringJoiner.addToken("take");

        assertThat(stringJoiner.getString(), is("let" + delimiter + "me" + delimiter + "take"));
    }

    @Test
    public void testRemoveToken() {
        stringJoiner.addToken("let");
        stringJoiner.addToken("me");
        stringJoiner.addToken("let");
        stringJoiner.addToken("take");
        stringJoiner.addToken("let");

        stringJoiner.removeToken("let");
        String expectedResult = "me" + delimiter + "take";

        assertThat(stringJoiner.getString(), is(expectedResult));
    }
}
