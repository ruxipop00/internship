package com.tora;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestStringTokenizer {
    private StringTokenizer stringTokenizer;
    String testString = "i have; so many apples";

    @Before
    public void setup() {
        stringTokenizer = new StringTokenizer(testString);
    }

    @Test
    public void testGetTokens() {
        List<String> tokens = stringTokenizer.getTokens();

        assertThat(tokens.get(0), is("i"));
        assertThat(tokens.get(1), is("have;"));
        assertThat(tokens.get(2), is("so"));
        assertThat(tokens.get(3), is("many"));
        assertThat(tokens.get(4), is("apples"));
    }

    @Test
    public void testGetTokenAt() {
        assertThat(stringTokenizer.getTokenAt(0), is("i"));
        assertThat(stringTokenizer.getTokenAt(1), is("have;"));
        assertThat(stringTokenizer.getTokenAt(2), is("so"));
        assertThat(stringTokenizer.getTokenAt(3), is("many"));
        assertThat(stringTokenizer.getTokenAt(4), is("apples"));
    }

    @Test
    public void testCountTokens() {
        assertThat(stringTokenizer.countTokens(), is(5));

        stringTokenizer.setString("not so many now");
        assertThat(stringTokenizer.countTokens(), is(4));
    }

    @Test
    public void testSetDelimiter() {
        stringTokenizer.setDelimiter("+-");
        stringTokenizer.setString("i+wanna+-go-home+-now");

        assertThat(stringTokenizer.countTokens(), is(3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testThrowExceptionForOutOfBoundsIndex() {
        stringTokenizer.setString("simple is the way to go");
        stringTokenizer.setDelimiter(" ");

        stringTokenizer.getTokenAt(10);
    }
}
