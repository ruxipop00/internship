package com.example.domain.binary;

import java.math.BigDecimal;
import java.math.MathContext;

public class Power implements BinaryOperation<BigDecimal> {
    @Override
    public BigDecimal calculate(BigDecimal x, BigDecimal y) {
        MathContext mc = new MathContext(4);
        return x.pow(y.intValueExact(), mc);
    }
}
