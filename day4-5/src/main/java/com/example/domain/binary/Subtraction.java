package com.example.domain.binary;

import java.math.BigDecimal;

public class Subtraction implements BinaryOperation<BigDecimal> {
    /*
    Class that represents the substraction operation.
    Overrides the method "calculate" from the Operation interface.
     */
    @Override
    public BigDecimal calculate(BigDecimal x, BigDecimal y) {
        /*
        Substracts two real numbers and returns the result.
         */
        return x.subtract(y);
    }
}
