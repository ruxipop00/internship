package com.example.domain.binary;

import java.math.BigDecimal;

public class Addition implements BinaryOperation<BigDecimal> {
    /*
    Class that represents the addition operation.
    Overrides the method "calculate" from the Operation interface.
     */
    @Override
    public BigDecimal calculate(BigDecimal x, BigDecimal y) {
        /*
        Adds two real numbers and returns the result.
         */
        return x.add(y);
    }
}
