package com.example.domain.binary;

import com.example.domain.validators.ValidatorException;

public interface BinaryOperation<T extends Number> {
    T calculate(T x, T y) throws ValidatorException;
}
