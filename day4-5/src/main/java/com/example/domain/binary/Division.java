package com.example.domain.binary;

import com.example.domain.validators.ValidatorException;

import java.math.BigDecimal;

public class Division implements BinaryOperation<BigDecimal> {
    /*
    Class that represents the division operation.
    Overrides the method "calculate" from the Operation interface.
     */
    @Override
    public BigDecimal calculate(BigDecimal x, BigDecimal y) throws ValidatorException {
        /*
        Divides two real numbers and returns the result.
        Throws exception if the second number is 0.
         */
        if (y.doubleValue() == 0) {
            throw new ValidatorException("\nError - Division by 0!\n");
        }
        return x.divide(y);
    }
}
