package com.example.domain.binary;

import java.math.BigDecimal;

public class Maximum implements BinaryOperation<BigDecimal> {
    @Override
    public BigDecimal calculate(BigDecimal x, BigDecimal y) {
        return x.max(y);
    }
}
