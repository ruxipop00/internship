package com.example.domain.binary;

import java.math.BigDecimal;

public class Multiplication implements BinaryOperation<BigDecimal> {
    /*
    Class that represents the multiplication operation.
    Overrides the method "calculate" from the Operation interface.
     */
    @Override
    public BigDecimal calculate(BigDecimal x, BigDecimal y) {
        /*
        Multiplies two real numbers and returns the result.
         */
        return x.multiply(y);
    }
}
