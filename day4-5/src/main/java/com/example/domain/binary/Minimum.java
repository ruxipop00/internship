package com.example.domain.binary;

import java.math.BigDecimal;

public class Minimum implements BinaryOperation<BigDecimal> {
    @Override
    public BigDecimal calculate(BigDecimal x, BigDecimal y) {
        return x.min(y);
    }
}
