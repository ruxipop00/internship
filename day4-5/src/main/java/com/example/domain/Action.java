package com.example.domain;

public enum Action {
    ADD("+"),
    SUBTRACT("-"),
    MULTIPLY("*"),
    DIVIDE("/"),
    MAXIMUM("max"),
    MINIMUM("min"),
    POWER("^"),
    ROUND("round"),
    SQRT("sqrt"),
    EXIT("exit");

    String operator;

    Action(String s) {
        this.operator = s;
    }

    public String getOperator() {
        return this.operator;
    }

}
