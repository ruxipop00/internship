package com.example.domain.unary;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Round implements UnaryOperation<BigDecimal> {
    @Override
    public BigDecimal calculate(BigDecimal x) {
        return x.setScale(0, RoundingMode.HALF_UP);
    }
}
