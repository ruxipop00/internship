package com.example.domain.unary;

import com.example.domain.validators.ValidatorException;

public interface UnaryOperation<T extends Number> {
    T calculate(T x) throws ValidatorException;
}
