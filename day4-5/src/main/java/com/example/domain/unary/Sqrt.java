package com.example.domain.unary;

import com.example.domain.validators.ValidatorException;

import java.math.BigDecimal;

public class Sqrt implements UnaryOperation<BigDecimal> {
    @Override
    public BigDecimal calculate(BigDecimal x) throws ValidatorException {
        if (x.doubleValue() < 0) {
            throw new ValidatorException("\nError - Number needs to be positive!\n");
        }
        return new BigDecimal(Math.sqrt(x.doubleValue()));
    }
}
