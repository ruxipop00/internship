package com.example.service;

import com.example.domain.Action;
import com.example.domain.binary.BinaryOperation;
import com.example.domain.unary.UnaryOperation;
import com.example.domain.validators.ValidatorException;
import com.example.factory.BinaryOperationFactory;
import com.example.factory.UnaryOperationFactory;

import java.math.BigDecimal;

public class CalculatorService {

    public BigDecimal calculateResult(BigDecimal x, BigDecimal y, Action action) throws ValidatorException, IllegalArgumentException {
        BinaryOperation<BigDecimal> binaryOperation = BinaryOperationFactory.getOperation(action);
        return binaryOperation.calculate(x, y);
    }

    public BigDecimal calculateResult(BigDecimal x, Action action) throws ValidatorException, IllegalArgumentException {
        UnaryOperation<BigDecimal> unaryOperation = UnaryOperationFactory.getOperation(action);
        return unaryOperation.calculate(x);
    }



//    public BigDecimal add(BigDecimal x, BigDecimal y) throws ValidatorException {
//        binaryOperation = new Addition();
//        return binaryOperation.calculate(x, y);
//    }
//
//    public BigDecimal subtract(BigDecimal x, BigDecimal y) throws ValidatorException {
//        binaryOperation = new Subtraction();
//        return binaryOperation.calculate(x, y);
//    }
//
//    public BigDecimal multiply(BigDecimal x, BigDecimal y) throws ValidatorException {
//        binaryOperation = new Multiplication();
//        return binaryOperation.calculate(x, y);
//    }
//
//    public BigDecimal divide(BigDecimal x, BigDecimal y) throws ValidatorException {
//        binaryOperation = new Division();
//        return binaryOperation.calculate(x, y);
//    }
//
//    public BigDecimal max(BigDecimal x, BigDecimal y) throws ValidatorException {
//        binaryOperation = new Maximum();
//        return binaryOperation.calculate(x, y);
//    }
//
//    public BigDecimal min(BigDecimal x, BigDecimal y) throws ValidatorException {
//        binaryOperation = new Minimum();
//        return binaryOperation.calculate(x, y);
//    }
//
//    public BigDecimal powerOf(BigDecimal x, BigDecimal y) throws ValidatorException {
//        binaryOperation = new Power();
//        return binaryOperation.calculate(x, y);
//    }
//
//    public BigDecimal round(BigDecimal x) throws ValidatorException {
//        unaryOperation = new Round();
//        return unaryOperation.calculate(x);
//    }
//
//    public BigDecimal sqrt(BigDecimal x) throws ValidatorException {
//        unaryOperation = new Sqrt();
//        return unaryOperation.calculate(x);
//    }
}
