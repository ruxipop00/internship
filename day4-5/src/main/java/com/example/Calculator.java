package com.example;

import com.example.domain.Action;
import com.example.domain.validators.ValidatorException;
import com.example.service.CalculatorService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Calculator {
    CalculatorService calculatorService;

    public Calculator(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    private static void isNumber(String nr) {
        if (!nr.matches("[-+]?[0-9]*\\.?[0-9]+")) {
            throw new IllegalArgumentException("\nError - Inputs have to be real numbers!\n");
        }
    }

    private static List<BigDecimal> readBinaryOperandsFromConsole() {
        Scanner scan = new Scanner(System.in);
        List<BigDecimal> params = new ArrayList<>();

        System.out.print("Enter the first number: ");
        String nr1 = scan.nextLine();
        Calculator.isNumber(nr1);
        BigDecimal numOne = new BigDecimal(nr1);
        params.add(numOne);

        System.out.print("Enter the second number: ");
        String nr2 = scan.nextLine();
        Calculator.isNumber(nr2);
        BigDecimal numTwo = new BigDecimal(nr2);
        params.add(numTwo);

        return params;
    }

    private static BigDecimal readUnaryOperandFromConsole() {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter the number: ");
        String nr = scan.nextLine();
        Calculator.isNumber(nr);

        return new BigDecimal(nr);
    }

    private static void showMenu() {
        int count = 0;

        for (Action action : Action.values()) {
            System.out.println(count + " - " + action);
            count += 1;
        }
    }

    private void addAction() throws ValidatorException {
        Action action = Action.ADD;
        List<BigDecimal> params;
        BigDecimal result;

        params = Calculator.readBinaryOperandsFromConsole();
        result = calculatorService.calculateResult(params.get(0), params.get(1), action);
        System.out.println("Result: " + params.get(0) + " + " + params.get(1) + " = " + result + "\n");
    }

    private void subtractAction() throws ValidatorException {
        Action action = Action.SUBTRACT;
        List<BigDecimal> params;
        BigDecimal result;

        params = Calculator.readBinaryOperandsFromConsole();
        result = calculatorService.calculateResult(params.get(0), params.get(1), action);
        System.out.println("Result: " + params.get(0) + " - " + params.get(1) + " = " + result + "\n");
    }

    private void multiplyAction() throws ValidatorException {
        Action action = Action.MULTIPLY;
        List<BigDecimal> params;
        BigDecimal result;

        params = Calculator.readBinaryOperandsFromConsole();
        result = calculatorService.calculateResult(params.get(0), params.get(1), action);
        System.out.println("Result: " + params.get(0) + " * " + params.get(1) + " = " + result + "\n");

    }

    private void divideAction() throws ValidatorException {
        Action action = Action.DIVIDE;
        List<BigDecimal> params;
        BigDecimal result;

        params = Calculator.readBinaryOperandsFromConsole();
        result = calculatorService.calculateResult(params.get(0), params.get(1), action);
        System.out.println("Result: " + params.get(0) + " / " + params.get(1) + " = " + result + "\n");

    }

    private void maxAction() throws ValidatorException {
        Action action = Action.MAXIMUM;
        List<BigDecimal> params;
        BigDecimal result;

        params = Calculator.readBinaryOperandsFromConsole();
        result = calculatorService.calculateResult(params.get(0), params.get(1), action);
        System.out.println("Result: max(" + params.get(0) + ", " + params.get(1) + ") => " + result + "\n");

    }

    private void minAction() throws ValidatorException {
        Action action = Action.MINIMUM;
        List<BigDecimal> params;
        BigDecimal result;

        params = Calculator.readBinaryOperandsFromConsole();
        result = calculatorService.calculateResult(params.get(0), params.get(1), action);
        System.out.println("Result: min(" + params.get(0) + ", " + params.get(1) + ") => " + result + "\n");

    }

    private void powerAction() throws ValidatorException {
        Action action = Action.POWER;
        List<BigDecimal> params;
        BigDecimal result;

        params = Calculator.readBinaryOperandsFromConsole();
        result = calculatorService.calculateResult(params.get(0), params.get(1), action);
        System.out.println("Result: " + params.get(0) + " ^ " + params.get(1) + " = " + result + "\n");

    }

    private void roundAction() throws ValidatorException {
        Action action = Action.ROUND;
        BigDecimal param;
        BigDecimal result;

        param = Calculator.readUnaryOperandFromConsole();
        result = calculatorService.calculateResult(param, action);
        System.out.println("Result: " + param + " => " + result + "\n");

    }

    private void sqrtAction() throws ValidatorException {
        Action action = Action.SQRT;
        BigDecimal param;
        BigDecimal result;

        param = Calculator.readUnaryOperandFromConsole();
        result = calculatorService.calculateResult(param, action);
        System.out.println("Result: sqrt(" + param + ") = " + result + "\n");

    }

    public void run() {
        Scanner scan = new Scanner(System.in);
        boolean calculatorOn = true;

        //TODO: while true -> break
        while (calculatorOn) {
            Calculator.showMenu();
            System.out.println(">> ");

            try {
                String option = scan.nextLine();
                Action action = Action.values()[Integer.parseInt(option)];

                switch (action) {
                    case ADD:
                        addAction();
                        break;
                    case SUBTRACT:
                        subtractAction();
                        break;
                    case MULTIPLY:
                        multiplyAction();
                        break;
                    case DIVIDE:
                        divideAction();
                        break;
                    case MAXIMUM:
                        maxAction();
                        break;
                    case MINIMUM:
                        minAction();
                        break;
                    case POWER:
                        powerAction();
                        break;
                    case ROUND:
                        roundAction();
                        break;
                    case SQRT:
                       sqrtAction();
                        break;
                    case EXIT:
                        calculatorOn = false;
                        break;
                    default:
                        throw new AssertionError("\nError - Unknown operation \n");
                }
            } catch (AssertionError | IllegalArgumentException | ValidatorException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
