package com.example.factory;

import com.example.domain.Action;
import com.example.domain.binary.*;

import java.math.BigDecimal;

public class BinaryOperationFactory {
    public static BinaryOperation<BigDecimal> getOperation(Action operationType) throws IllegalArgumentException {
        switch (operationType) {
            case ADD:
                return new Addition();
            case DIVIDE:
                return new Division();
            case MAXIMUM:
                return new Maximum();
            case MINIMUM:
                return new Minimum();
            case MULTIPLY:
                return new Multiplication();
            case POWER:
                return new Power();
            case SUBTRACT:
                return new Subtraction();
            default:
                throw new IllegalArgumentException("Incorrect operation type");
        }
    }
}
