package com.example.factory;

import com.example.domain.Action;
import com.example.domain.unary.Round;
import com.example.domain.unary.Sqrt;
import com.example.domain.unary.UnaryOperation;

import java.math.BigDecimal;

public class UnaryOperationFactory {
    public static UnaryOperation<BigDecimal> getOperation(Action operationType) throws IllegalArgumentException {
        switch (operationType) {
            case ROUND:
                return new Round();
            case SQRT:
                return new Sqrt();
            default:
                throw new IllegalArgumentException("Incorrect operation type");
        }
    }
}
