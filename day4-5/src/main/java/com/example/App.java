package com.example;

import com.example.service.CalculatorService;

public class App {
    public static void main(String[] args) {
        CalculatorService cs = new CalculatorService();
        Calculator calculator = new Calculator(cs);
        calculator.run();
    }
}
