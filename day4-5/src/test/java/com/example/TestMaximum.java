package com.example;

import com.example.domain.binary.BinaryOperation;
import com.example.domain.binary.Maximum;
import com.example.domain.validators.ValidatorException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestMaximum {
    private BinaryOperation<BigDecimal> maximum;

    @Before
    public void setup() {
        maximum = new Maximum();
    }

    @Test
    public void testMaximumResult() throws ValidatorException {
        BigDecimal x  = new BigDecimal(1001);
        BigDecimal y  = new BigDecimal(1002);

        assertThat(maximum.calculate(x, y), is(new BigDecimal(1002)));
    }
}
