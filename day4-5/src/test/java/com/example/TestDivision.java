package com.example;

import com.example.domain.binary.BinaryOperation;
import com.example.domain.binary.Division;
import com.example.domain.validators.ValidatorException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestDivision {
    private BinaryOperation<BigDecimal> division;

    @Before
    public void setup() {
        division = new Division();
    }

    @Test
    public void testDivisionResult() throws ValidatorException {
        BigDecimal x  = new BigDecimal(4);
        BigDecimal y  = new BigDecimal(2);

        assertThat(division.calculate(x, y), is(new BigDecimal(2)));
    }

    @Test(expected = ValidatorException.class)
    public void throwExceptionForDivisionByZero() throws ValidatorException {
        BigDecimal x  = new BigDecimal(4);
        BigDecimal y  = new BigDecimal(0);

        division.calculate(x, y);
    }
}
