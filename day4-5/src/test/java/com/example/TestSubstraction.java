package com.example;

import com.example.domain.binary.BinaryOperation;
import com.example.domain.binary.Subtraction;
import com.example.domain.validators.ValidatorException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestSubstraction {
    private BinaryOperation<BigDecimal> subtraction;

    @Before
    public void setup() {
        subtraction = new Subtraction();
    }

    @Test
    public void testSubstractionResult() throws ValidatorException {
        BigDecimal x  = new BigDecimal(10);
        BigDecimal y  = new BigDecimal(2);

        assertThat(subtraction.calculate(x, y), is(new BigDecimal(8)));
    }
}
