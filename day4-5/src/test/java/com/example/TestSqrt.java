package com.example;

import com.example.domain.unary.Sqrt;
import com.example.domain.unary.UnaryOperation;
import com.example.domain.validators.ValidatorException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestSqrt {
    private UnaryOperation<BigDecimal> squareRoot;

    @Before
    public void setup() {
        squareRoot = new Sqrt();
    }

    @Test
    public void testRoundResult() throws ValidatorException {
        BigDecimal x  = new BigDecimal(16);

        assertThat(squareRoot.calculate(x), is(new BigDecimal(4)));
    }

    @Test(expected = ValidatorException.class)
    public void throwExceptionForNegativeNumber() throws ValidatorException {
        BigDecimal x  = new BigDecimal(-16);

        squareRoot.calculate(x);
    }
}
