package com.example;

import com.example.domain.binary.BinaryOperation;
import com.example.domain.binary.Multiplication;
import com.example.domain.validators.ValidatorException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestMultiplication {
    private BinaryOperation<BigDecimal> multiplication;

    @Before
    public void setup() {
        multiplication = new Multiplication();
    }

    @Test
    public void testMultiplicationResult() throws ValidatorException {
        BigDecimal x  = new BigDecimal(1);
        BigDecimal y  = new BigDecimal(2);

        assertThat(multiplication.calculate(x, y), is(new BigDecimal(2)));
    }
}
