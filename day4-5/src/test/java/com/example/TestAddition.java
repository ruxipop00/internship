package com.example;

import com.example.domain.binary.Addition;
import com.example.domain.binary.BinaryOperation;
import com.example.domain.validators.ValidatorException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestAddition {
    private BinaryOperation<BigDecimal> addition;

    @Before
    public void setup() {
        addition = new Addition();
    }

    @Test
    public void testAdditionResult() throws ValidatorException {
        BigDecimal x  = new BigDecimal(1);
        BigDecimal y  = new BigDecimal(2);

        assertThat(addition.calculate(x, y), is(new BigDecimal(3)));
    }
}
