package com.example;

import com.example.domain.binary.BinaryOperation;
import com.example.domain.binary.Power;
import com.example.domain.validators.ValidatorException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestPower {
    private BinaryOperation<BigDecimal> power;

    @Before
    public void setup() {
        power = new Power();
    }

    @Test
    public void testPowerResult() throws ValidatorException {
        BigDecimal x  = new BigDecimal(2);
        BigDecimal y  = new BigDecimal(3);

        assertThat(power.calculate(x, y), is(new BigDecimal(8)));
    }
}
