package com.example;

import com.example.domain.unary.Round;
import com.example.domain.unary.UnaryOperation;
import com.example.domain.validators.ValidatorException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestRound {
    private UnaryOperation<BigDecimal> rounding;

    @Before
    public void setup() {
        rounding = new Round();
    }

    @Test
    public void testRoundResult() throws ValidatorException {
        BigDecimal x  = new BigDecimal("10.213");

        assertThat(rounding.calculate(x), is(new BigDecimal(10)));
    }
}
