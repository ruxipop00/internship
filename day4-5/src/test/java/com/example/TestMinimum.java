package com.example;

import com.example.domain.binary.BinaryOperation;
import com.example.domain.binary.Minimum;
import com.example.domain.validators.ValidatorException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestMinimum {
    private BinaryOperation<BigDecimal> minimum;

    @Before
    public void setup() {
        minimum = new Minimum();
    }

    @Test
    public void testMinimumResult() throws ValidatorException {
        BigDecimal x  = new BigDecimal("1001.2");
        BigDecimal y  = new BigDecimal(1002);

        assertThat(minimum.calculate(x, y), is(new BigDecimal("1001.2")));
    }
}
