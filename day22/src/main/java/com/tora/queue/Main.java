package com.tora.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    public static void main(String[] args) {
        BlockingQueue<Integer> blockingQueue = new LinkedBlockingQueue<>(5);

        Producer p = new Producer(blockingQueue);
        Consumer c = new Consumer(blockingQueue);

        p.start();
        c.start();
    }
}
