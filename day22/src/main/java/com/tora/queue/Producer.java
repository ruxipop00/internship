package com.tora.queue;

import java.util.concurrent.BlockingQueue;

public class Producer extends Thread {
    private BlockingQueue<Integer> blockingQueue;

    public Producer(BlockingQueue<Integer> blockingQueue) {
        super("Producer");
        this.blockingQueue = blockingQueue;
    }

    public void run() {
        for (int i = 0; i < 20; i++) {
            try {
                blockingQueue.put(i);
                System.out.println(getName() + " produced " + i);
//                if (i == 5) {
//                    Thread.sleep(200);
//                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
