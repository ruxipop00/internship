package com.tora.queue;

import java.util.concurrent.BlockingQueue;

public class Consumer extends Thread {
    private BlockingQueue<Integer> blockingQueue;

    public Consumer(BlockingQueue<Integer> blockingQueue) {
        super("Consumer");
        this.blockingQueue = blockingQueue;
    }

    public void run() {
        try {
            Thread.sleep(200);
            while (true) {
                Integer elem = blockingQueue.take();
                System.out.println(getName() + " consumed " + elem);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
