import {Component, Input, OnInit} from '@angular/core';
import {OfficeSpace} from "../shared/models/office.model";
import {OfficeService} from "../shared/office.service";
import {ActivatedRoute, Params} from "@angular/router";
import {switchMap} from "rxjs/operators";
import {Seat} from "../shared/models/seat.model";
import {SeatService} from "../shared/seat.service";
import {ReservationService} from "../shared/reservation.service";
import {AuthService} from "../shared/auth/auth.service";

@Component({
  selector: 'app-office-details',
  templateUrl: './office-details.component.html',
  styleUrls: ['./office-details.component.css']
})
export class OfficeDetailsComponent implements OnInit {

  @Input() office: OfficeSpace;
  myFilter = (d: Date | null): boolean => {
    const day = (d || new Date()).getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  };
  seats: Array<Seat>;
  selectedSeat: String;
  selectedDate: String;

  constructor(private officeService: OfficeService,
              private seatService: SeatService,
              private reservationService: ReservationService,
              private authService: AuthService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params
      .pipe(switchMap((params: Params) => this.officeService.getOfficeSpace(+params['id'])))
      .subscribe(office => {this.office = office;
                                  this.seatService.getSeatsFromOffice(this.office.id)
                                    .subscribe(
                                      seats => this.seats = seats,
                                  );});

    // this.getSeats();
  }

  getSeats() {
    this.seatService.getSeatsFromOffice(this.office.id)
      .subscribe(
        seats => this.seats = seats,
      );
  }

  makeReservation() {
    const userId = this.authService.currentUser;
    this.reservationService.addReservation({
      id: 0,
      userId: +userId,
      seatId: +this.selectedSeat,
      date: this.selectedDate.valueOf(),
    })
      .subscribe(status => console.log("saved reservation: ", status));
  }

  onDateChange(event) {
    this.selectedDate = event;
  }

  onSeatChange(event) {
    this.selectedSeat = event;
  }

}

