import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MyUser} from "../shared/models/user.model";
import {AuthService} from "../shared/auth/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  user: MyUser;
  errorMessage = false;

  constructor(private authService: AuthService,
              private formBuilder: FormBuilder,
              private router: Router) {
    this.user = new MyUser();
  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.loginForm.valueChanges.subscribe(values => {
      this.user.email = values.username;
      this.user.password = values.password;
    });
  }

  login(): void {
    this.authService.login(this.user)
      .subscribe(() => this.successLogin(),
        () => this.errorMessage = true);
  }

  private successLogin(): void {
    this.router.navigate(['']);
  }
}

