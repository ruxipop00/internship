import { Component } from '@angular/core';
import {AuthService} from "./shared/auth/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'officeSpaceAppp';

  constructor(public authService: AuthService) {
  }

  get isLoggedIn(): boolean {
    return this.authService.loggedIn();
  }

  logout(): void {
    this.authService.logout();
    window.location.reload();
  }
}
