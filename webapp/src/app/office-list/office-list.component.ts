import { Component, OnInit } from '@angular/core';
import {MyUser} from "../shared/models/user.model";
import {OfficeSpace} from "../shared/models/office.model";
import {Router} from "@angular/router";
import {OfficeService} from "../shared/office.service";

@Component({
  moduleId: module.id,
  selector: 'app-office-list',
  templateUrl: './office-list.component.html',
  styleUrls: ['./office-list.component.css']
})

export class OfficeListComponent implements OnInit {
  officeSpaces: Array<OfficeSpace>;
  user: MyUser;

  constructor(private officeSpaceService: OfficeService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getOfficeSpaces();
  }

  getOfficeSpaces() {
    this.officeSpaceService.getOfficeSpaces()
      .subscribe(
        officeSpaces => this.officeSpaces = officeSpaces,
      );
  }

  gotoDetail(office: OfficeSpace): void {
    this.router.navigate(['/office', office.id]);
  }
}
