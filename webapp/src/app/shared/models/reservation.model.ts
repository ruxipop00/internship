export class Reservation {
  id: number;
  userId: number;
  seatId: number;
  date: string;
}
