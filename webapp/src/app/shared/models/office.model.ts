export class OfficeSpace {
  id: number;
  name: string;
  location: string;
}
