export class MyUser {
  id: number;
  name: string;
  email: string;
  password: string;
}
