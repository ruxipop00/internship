import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {OfficeSpace} from "./models/office.model";
import {Injectable} from "@angular/core";
import {map} from "rxjs/operators";

@Injectable()
export class OfficeService {
  private officesUrl = 'http://localhost:8080/api/offices';

  constructor(private httpClient: HttpClient) {
  }

  getOfficeSpaces(): Observable<OfficeSpace[]> {
    return this.httpClient
      .get<Array<OfficeSpace>>(this.officesUrl);
  }

  getOfficeSpace(id: number): Observable<OfficeSpace> {
    return this.getOfficeSpaces()
      .pipe(
        map(officeSpaces => officeSpaces.find(office => office.id === id))
      );
  }
}
