import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MyUser} from "../models/user.model";
import {tap} from 'rxjs/operators';
import {Auth} from "../models/auth.model";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl = 'http://localhost:8080/api';

  constructor(private httpClient: HttpClient) {
  }

  login(user: Auth): Observable<any> {
    return this.httpClient.post( `${this.baseUrl}/auth/login`, user)
      .pipe(tap(u => AuthService.setSession(u)));
  }

  loggedIn(): boolean {
    return !!localStorage.getItem('logged');
  }

  logout(): void {
    localStorage.removeItem('logged');
    localStorage.removeItem('username');
    localStorage.removeItem('expires_at');
  }

  get currentUser(): string {
    return localStorage.getItem('id');
  }

  private static setSession(user: MyUser): void {
    localStorage.setItem('logged', 'true');
    localStorage.setItem('id', String(user.id));
    localStorage.setItem('username', user.email);
    localStorage.setItem('username', user.name);
  }
}
