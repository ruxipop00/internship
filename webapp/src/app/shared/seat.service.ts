import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Seat} from "./models/seat.model";

@Injectable()
export class SeatService {
  private seatsUrl = 'http://localhost:8080/api/seats';

  constructor(private httpClient: HttpClient) {
  }

  getAllSeats(): Observable<Seat[]> {
    return this.httpClient
      .get<Array<Seat>>(this.seatsUrl);
  }

  getSeatsFromOffice(officeId: number): Observable<Seat[]> {
    const url = `${this.seatsUrl}/${officeId}`;

    return this.httpClient
      .get<Array<Seat>>(url);
  }
}
