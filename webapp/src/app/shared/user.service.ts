import {Injectable} from '@angular/core';

import {HttpClient} from "@angular/common/http";

import {MyUser} from "./models/user.model";

import {Observable} from "rxjs";
import {Auth} from "./models/auth.model";


@Injectable()
export class UserService {
  private usersUrl = 'http://localhost:8080/api/users';

  constructor(private httpClient: HttpClient) {
  }

  // getMyUsers(): Observable<MyUser[]> {
  //   return this.httpClient
  //     .get<Array<MyUser>>(this.usersUrl);
  // }
  //
  // getMyUser(id: number): Observable<MyUser> {
  //   return this.getMyUsers()
  //     .pipe(
  //       map(users => users.find(user => user.id === id))
  //     );
  // }
  //
  // saveMyUser(user: MyUser): Observable<MyUser> {
  //   console.log("saveMyUser", user);
  //
  //   return this.httpClient
  //     .post<MyUser>(this.usersUrl, user);
  // }

  loginUser(user: Auth): Observable<MyUser> {
    console.log("loginUser", user);
    const url = `${this.usersUrl}/auth/login`;

    return this.httpClient
      .post<MyUser>(this.usersUrl, user);
  }

  // update(user): Observable<MyUser> {
  //   const url = `${this.usersUrl}/${user.id}`;
  //   return this.httpClient
  //     .put<MyUser>(url, user);
  // }
  //
  // deleteMyUser(id: number): Observable<any> {
  //   const url = `${this.usersUrl}/${id}`;
  //   return this.httpClient
  //     .delete(url);
  // }

}
