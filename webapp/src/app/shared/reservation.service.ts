import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Seat} from "./models/seat.model";
import {Reservation} from "./models/reservation.model";

@Injectable()
export class ReservationService {
  private reservationsUrl = 'http://localhost:8080/api/reserve';

  constructor(private httpClient: HttpClient) {
  }

  getAllSeats(): Observable<Seat[]> {
    return this.httpClient
      .get<Array<Seat>>(this.reservationsUrl);
  }

  addReservation(reservation: Reservation): Observable<any> {
    return this.httpClient
      .post<any>(this.reservationsUrl, reservation);
  }
}
