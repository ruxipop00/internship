import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";

import { LoginComponent } from './login/login.component';
import {AuthService} from "./shared/auth/auth.service";
import {AuthGuardService} from "./shared/auth/auth-guard.service";
import {AlreadyLoggedGuardService} from "./shared/auth/already-logged-guard.service";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import { OfficeListComponent } from './office-list/office-list.component';
import {MatCardModule} from "@angular/material/card";
import {UserService} from "./shared/user.service";
import {OfficeService} from "./shared/office.service";
import { OfficeDetailsComponent } from './office-details/office-details.component';
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {SeatService} from "./shared/seat.service";
import {MatSelectModule} from "@angular/material/select";
import {ReservationService} from "./shared/reservation.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OfficeListComponent,
    OfficeDetailsComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule
  ],
  providers: [AuthService, AuthGuardService, AlreadyLoggedGuardService, UserService, OfficeService, SeatService, ReservationService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
