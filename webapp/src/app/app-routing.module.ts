import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {AlreadyLoggedGuardService} from "./shared/auth/already-logged-guard.service";
import {OfficeListComponent} from "./office-list/office-list.component";
import {AuthGuardService} from "./shared/auth/auth-guard.service";
import {OfficeDetailsComponent} from "./office-details/office-details.component";


const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'login', component: LoginComponent, canActivate: [AlreadyLoggedGuardService]},
  {path: 'officeSpaces', component: OfficeListComponent, canActivate: [AuthGuardService]},
  {path: 'office/:id', component: OfficeDetailsComponent, canActivate: [AuthGuardService]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
