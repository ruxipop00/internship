# internship
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/76eabf38072f4e9791497c0bfbfc309d)](https://www.codacy.com/manual/ruxipop00/internship?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=ruxipop00/internship&amp;utm_campaign=Badge_Grade)

Hackaton project

- day18-ruxi-core
- day18-ruxi-web
- webapp

- create database "tora" in postgresql with username and password from day18-ruxi-web/src/main/resources/local/db.properties OR create any database and modify properties in db.properties file
- add tomcat configuration for day18-ruxi-web:war artifact
- add angular configuration
- run tomcat
- run angular configuration (Angular CLI Server)
