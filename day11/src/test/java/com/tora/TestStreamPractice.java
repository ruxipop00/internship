package com.tora;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestStreamPractice {
    private StreamPractice streamPractice;

    @Before
    public void setup() {
        streamPractice = new StreamPractice();
    }

    @Test
    public void testSumResult() {
        streamPractice.add(new BigDecimal(10));
        streamPractice.add(new BigDecimal(14));
        streamPractice.add(new BigDecimal(6));
        streamPractice.add(new BigDecimal(11));
        BigDecimal expectedResult = new BigDecimal(41);

        assertThat(streamPractice.sum(), is(expectedResult));
    }

    @Test
    public void testAverageResult() {
        streamPractice.add(new BigDecimal(10));
        streamPractice.add(new BigDecimal(14));
        streamPractice.add(new BigDecimal(6));
        streamPractice.add(new BigDecimal(11));
        BigDecimal expectedResult = new BigDecimal("10.25");

        assertThat(streamPractice.average(), is(expectedResult));
    }

    @Test
    public void testTopTenBiggestNumbers() {
        streamPractice.add(new BigDecimal(10));
        streamPractice.add(new BigDecimal(14));
        streamPractice.add(new BigDecimal(6));
        streamPractice.add(new BigDecimal(11));
        BigDecimal expectedResult = new BigDecimal(14);

        assertThat(streamPractice.topTenBiggestNumbers().get(0), is(expectedResult));
    }

    @Test
    public void testTopTenBiggestNumbersLargeList() {
        streamPractice.add(new BigDecimal(10));
        streamPractice.add(new BigDecimal(14));
        streamPractice.add(new BigDecimal(6));
        streamPractice.add(new BigDecimal(21));
        streamPractice.add(new BigDecimal(121));
        streamPractice.add(new BigDecimal(111));
        streamPractice.add(new BigDecimal(121));
        streamPractice.add(new BigDecimal(1331));
        streamPractice.add(new BigDecimal(111));
        streamPractice.add(new BigDecimal(1));
        streamPractice.add(new BigDecimal(21));

        assertThat(streamPractice.topTenBiggestNumbers().get(0), is(new BigDecimal("1331")));
        assertThat(streamPractice.topTenBiggestNumbers().get(1), is(new BigDecimal("121")));
    }

    @Test
    public void testTopTenBiggestNumbersAddNullScenario() {
        streamPractice.add(new BigDecimal(10));
        streamPractice.add(null);
        streamPractice.add(new BigDecimal(6));
        BigDecimal expectedResult = new BigDecimal(10);

        assertThat(streamPractice.topTenBiggestNumbers().get(0), is(expectedResult));
    }

    @Test
    public void testSize() {
        streamPractice.add(new BigDecimal(10));
        streamPractice.add(null);

        assertThat(streamPractice.size(), is(2));
    }


}
