package com.tora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class StreamPractice {
    List<BigDecimal> myList;

    public StreamPractice() {
        myList = new ArrayList<>();
    }

    public BigDecimal sum() {
        return myList.stream()
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal average() {
        return sum()
                .divide(new BigDecimal(size()), 2, RoundingMode.HALF_UP);
    }

    public List<BigDecimal> topTenBiggestNumbers() {
        long myLimit = (long) Math.ceil((0.1 * (double) myList.size()));

         return myList.stream()
                 .filter(Objects::nonNull)
                 .sorted((e1, e2) -> e2.compareTo(e1))
                 .limit(myLimit)
                 .collect(Collectors.toList());
    }

    public int size() {
        return (int) myList.stream()
                .count();
    }

    public void add(BigDecimal e) {
        myList.add(e);
    }
}
