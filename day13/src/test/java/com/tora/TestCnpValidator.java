package com.tora;

import com.tora.validators.CnpValidator;
import com.tora.validators.Validator;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestCnpValidator {
    private Validator validator;
    private Person person;

    private String firstName = "Sarah";
    private String midName = "Susan";
    private String lastName = "Gerald";
    private String email = "Sarah.Susan.Gerald@yahoo.com";

    @Before
    public void setup() {
        validator = new CnpValidator(null);

        String cnp = "6000125125771";
        person = new Person(firstName, lastName, midName, cnp, email);
    }

    @Test
    public void testValidateCnp() {
        assertThat(validator.validate(person), is(true));
        assertThat(validator.validate(new Person(firstName, lastName, midName, "1222", email)), is(false));
        assertThat(validator.validate(new Person(firstName, lastName, midName, "asswwaaaaaaaa", email)), is(false));
        assertThat(validator.validate(new Person(firstName, lastName, midName, "..", email)), is(false));
    }
}
