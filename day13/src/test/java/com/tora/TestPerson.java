package com.tora;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestPerson {
    private Person person;

    private String firstName = "Sarah";
    private String midName = "Susan";
    private String lastName = "Gerald";
    private String cnp = "6000125125771";
    private String email = "Sarah.Susan.Gerald@yahoo.com";

    @Before
    public void setup() {
        person = new Person(firstName, lastName, midName, cnp, email);
    }

    @Test
    public void testPersonGetters() {
        assertThat(person.getFirstName(), is(firstName));
        assertThat(person.getMidName(), is(midName));
        assertThat(person.getLastName(), is(lastName));
        assertThat(person.getCnp(), is(cnp));
        assertThat(person.getEmail(), is(email));
    }
}
