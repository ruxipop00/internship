package com.tora;

import com.tora.validators.EmailValidator;
import com.tora.validators.Validator;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestEmailValidator {
    private Validator validator;
    private Person person;

    private String firstName = "Sarah";
    private String midName = "Susan";
    private String lastName = "Gerald";
    private String cnp = "6000125125771";

    @Before
    public void setup() {
        validator = new EmailValidator(null);

        String email = "Sarah.Susan.Gerald@yahoo.com";
        person = new Person(firstName, lastName, midName, cnp, email);
    }

    @Test
    public void testValidateCnp() {
        assertThat(validator.validate(person), is(true));
        assertThat(validator.validate(new Person(firstName, lastName, midName, cnp, "@yahoo.com")), is(false));
        assertThat(validator.validate(new Person(firstName, lastName, midName, cnp, "ana.maria@yahoo.com")), is(false));
        assertThat(validator.validate(new Person(firstName, lastName, midName, cnp, "sa@yahoo.com")), is(false));
    }
}
