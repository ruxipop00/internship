package com.tora;

import com.tora.validators.NameValidator;
import com.tora.validators.Validator;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestNameValidator {
    private Validator validator;
    private Person person;

    private String firstName = "Sarah";
    private String midName = "Susan";
    private String lastName = "Gerald";
    private String cnp = "6000125125771";
    private String email = "Sarah.Susan.Gerald@yahoo.com";

    @Before
    public void setup() {
        validator = new NameValidator(null);
        person = new Person(firstName, lastName, midName, cnp, email);
    }

    @Test
    public void testValidateName() {
        assertThat(validator.validate(person), is(true));
        assertThat(validator.validate(new Person(firstName, "21", midName, cnp, email)), is(false));
        assertThat(validator.validate(new Person("21", lastName, midName, cnp, email)), is(false));
        assertThat(validator.validate(new Person(firstName, lastName, "21", cnp, email)), is(false));
    }
}
