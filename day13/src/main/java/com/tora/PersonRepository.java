package com.tora;

import java.util.ArrayList;
import java.util.List;

public class PersonRepository {
    private List<Person> persons;

    public PersonRepository() {
        this.persons = new ArrayList<>();
    }

    public synchronized void addPerson(Person person) {
        persons.add(person);
    }

    public List<Person> getPersons() {
        return persons;
    }
}
