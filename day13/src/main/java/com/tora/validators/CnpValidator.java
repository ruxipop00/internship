package com.tora.validators;

import com.tora.Person;

public class CnpValidator extends Validator {
    private static final String cnpRegex = "^\\d{13}$";

    public CnpValidator(Validator validator) {
        super(validator);
    }

    public boolean validate(Person person) {
        if (isValid(cnpRegex, person.getCnp())) {
            return super.validate(person);
        } else {
            return false;
        }
    }
}
