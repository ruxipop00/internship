package com.tora.validators;

import com.tora.Person;

public class EmailValidator extends Validator {
    private static final String emailRegex = "^\\w+\\.\\w+\\.\\w+@\\w+\\.com";

    public EmailValidator(Validator validator) {
        super(validator);
    }

    public boolean validate(Person person) {
        if (isValid(emailRegex, person.getEmail())) {
            return super.validate(person);
        } else {
            return false;
        }
    }
}
