package com.tora.validators;

import com.tora.Person;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Validator {
    private Validator nextValidator;

    public Validator(Validator validator) {
        this.nextValidator = validator;
    }

    public boolean validate(Person person) {
        if (this.nextValidator != null) {
            return this.nextValidator.validate(person);
        }
        return true;
    }

    public boolean isValid(String regex, String input) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        return matcher.matches();
    }
}
