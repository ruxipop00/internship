package com.tora.validators;

import com.tora.Person;

public class NameValidator extends Validator {
    private static final String nameRegex = "^[A-Z][a-z]+$";

    public NameValidator(Validator validator) {
        super(validator);
    }

    public boolean validate(Person person) {
        if (isValid(nameRegex, person.getFirstName()) && isValid(nameRegex, person.getMidName()) && isValid(nameRegex, person.getLastName())) {
            return super.validate(person);
        } else {
            return false;
        }
    }
}
