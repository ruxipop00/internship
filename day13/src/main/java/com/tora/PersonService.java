package com.tora;

import com.tora.validators.Validator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class PersonService {
    private PersonRepository repository;
    private Validator validator;

    public PersonService(PersonRepository repository, Validator validator) {
        this.repository = repository;
        this.validator = validator;
    }

    public void loadData(String filename) throws IOException {
        Path path = Paths.get(filename);
        StringBuilder sb = new StringBuilder();

        Files.lines(path).forEach(sb::append);
        String fileContents = sb.toString();

        List<String> people = Arrays.asList(fileContents.split("#"));

        people.forEach(person -> {
            List<String> tokens = Arrays.asList(person.split("\\|"));

            String firstName = tokens.get(0);
            String midName = tokens.get(1);
            String lastName = tokens.get(2);
            String cnp = tokens.get(3);
            String email = tokens.get(4);

            Person newPerson = new Person(firstName, lastName, midName, cnp, email);

            if (validator.validate(newPerson)) {
                repository.addPerson(newPerson);
            }

        });
    }

    public void loadSerializedData(String filename) throws IOException, ClassNotFoundException {

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File(filename)))) {
            Person person;
            while ((person = (Person) ois.readObject()) != null) {
                if (validator.validate(person)) {
                    repository.addPerson(person);
                }
            }
        } catch (EOFException ignored) {
        }
    }

    public void writeToFileSerialized(String filename) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(filename)));
        List<Person> people = repository.getPersons();

        for (Person p : people) {
            oos.writeObject(p);
        }

        oos.close();
    }

    public List<Person> getPersons() {
        return repository.getPersons();
    }

    public PersonRepository getRepository() {
        return repository;
    }

    public Validator getValidator() {
        return validator;
    }
}
