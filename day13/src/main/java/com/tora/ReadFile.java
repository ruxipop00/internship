package com.tora;

import com.tora.validators.Validator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class ReadFile implements Runnable {
    private String filename;
    private Validator validator;
    private PersonRepository repository;

    public ReadFile(String filename, Validator validator, PersonRepository repository) {
        this.filename = filename;
        this.validator = validator;
        this.repository = repository;
    }

    @Override
    public void run() {
        Path path = Paths.get(filename);
        StringBuilder sb = new StringBuilder();

        try {
            Files.lines(path).forEach(sb::append);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String fileContents = sb.toString();

        List<String> people = Arrays.asList(fileContents.split("#"));

        people.forEach(person -> {
            List<String> tokens = Arrays.asList(person.split("\\|"));

            String firstName = tokens.get(0);
            String midName = tokens.get(1);
            String lastName = tokens.get(2);
            String cnp = tokens.get(3);
            String email = tokens.get(4);

            Person newPerson = new Person(firstName, lastName, midName, cnp, email);

            if (validator.validate(newPerson)) {
                repository.addPerson(newPerson);
            }

        });
    }
}
