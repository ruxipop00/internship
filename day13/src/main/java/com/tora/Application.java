package com.tora;

import java.util.ArrayList;
import java.util.List;

public class Application {
    private PersonService personService;

    public Application(PersonService personService) {
        this.personService = personService;
    }

    public void threadPhase1() {
        List<String> filenames = new ArrayList<>();
        filenames.add("day13/src/main/java/com/tora/input/day13_input0.txt");
        filenames.add("day13/src/main/java/com/tora/input/day13_input1.txt");
        filenames.add("day13/src/main/java/com/tora/input/day13_input2.txt");
        filenames.add("day13/src/main/java/com/tora/input/day13_input3.txt");

        long end = 0;
        long start = System.currentTimeMillis();

        Runnable runnable = new ReadAllFiles(filenames, personService.getValidator(), personService.getRepository());

        Thread thread = new Thread(runnable);
        thread.start();

        try {
            thread.join();
            end = System.currentTimeMillis();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Start: " + start);
        System.out.println("End: " + end);
        System.out.println("Time: " + (end - start));
        System.out.println("List size: " + personService.getPersons().size());
    }

    public void threadPhase2() {
        List<String> filenames = new ArrayList<>();
        filenames.add("day13/src/main/java/com/tora/input/day13_input0.txt");
        filenames.add("day13/src/main/java/com/tora/input/day13_input1.txt");
        filenames.add("day13/src/main/java/com/tora/input/day13_input2.txt");
        filenames.add("day13/src/main/java/com/tora/input/day13_input3.txt");

        List<Thread> threads = new ArrayList<>();
        long end = 0;
        long start = System.currentTimeMillis();

        for (String file : filenames) {
            Runnable runnable = new ReadFile(file, personService.getValidator(), personService.getRepository());
            Thread thread = new Thread(runnable);
            threads.add(thread);
        }

        for(Thread thread : threads) {
            thread.start();
        }

        try {
            for(Thread thread : threads) {
                thread.join();
            }
            end = System.currentTimeMillis();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Start: " + start);
        System.out.println("End: " + end);
        System.out.println("Time: " + (end - start));
        System.out.println("List size: " + personService.getPersons().size());
    }
}
