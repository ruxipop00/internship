package com.tora;

import java.io.Serializable;

public class Person implements Serializable {
    private static final long serialVersionUID = 4L;

    private final String firstName;
    private final String lastName;
    private final String midName;
    private final String cnp;
    private final String email;
    private int age;

    public Person(String firstName, String lastName, String midName, String cnp, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.midName = midName;
        this.cnp = cnp;
        this.email = email;
    }

    public Person(String firstName, String lastName, String midName, String cnp, String email, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.midName = midName;
        this.cnp = cnp;
        this.email = email;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMidName() {
        return midName;
    }

    public String getCnp() {
        return cnp;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", midName='" + midName + '\'' +
                ", cnp='" + cnp + '\'' +
                ", email='" + email +
                '}';
    }
}
