package com.tora;

import com.tora.validators.CnpValidator;
import com.tora.validators.EmailValidator;
import com.tora.validators.NameValidator;
import com.tora.validators.Validator;

public class Main {
    public static void main(String[] args) {
        PersonRepository repository = new PersonRepository();
        Validator validator = new NameValidator(new CnpValidator(new EmailValidator(null)));

        PersonService personService = new PersonService(repository, validator);
        Application app = new Application(personService);

        app.threadPhase1();
    }
}
