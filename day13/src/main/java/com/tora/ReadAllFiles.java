package com.tora;

import com.tora.validators.Validator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class ReadAllFiles implements Runnable {
    private List<String> filenames;
    private Validator validator;
    private PersonRepository repository;

    public ReadAllFiles(List<String> filenames, Validator validator, PersonRepository repository) {
        this.filenames = filenames;
        this.validator = validator;
        this.repository = repository;
    }

    @Override
    public void run() {
        for (String file : filenames) {
            Path path = Paths.get(file);
            StringBuilder sb = new StringBuilder();

            try {
                Files.lines(path).forEach(sb::append);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String fileContents = sb.toString();

            List<String> people = Arrays.asList(fileContents.split("#"));

            people.forEach(person -> {
                List<String> tokens = Arrays.asList(person.split("\\|"));

                String firstName = tokens.get(0);
                String midName = tokens.get(1);
                String lastName = tokens.get(2);
                String cnp = tokens.get(3);
                String email = tokens.get(4);

                Person newPerson = new Person(firstName, lastName, midName, cnp, email);

                if (validator.validate(newPerson)) {
                    repository.addPerson(newPerson);
                }

            });
        }
    }
}
