package com.tora.core.repository;

import com.tora.core.model.OfficeSpace;

public interface OfficeSpaceRepository extends IRepository<OfficeSpace, Long> {
}