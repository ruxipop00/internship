package com.tora.core.repository;

import com.tora.core.model.Seat;

public interface SeatRepository extends IRepository<Seat, Long> {
}
