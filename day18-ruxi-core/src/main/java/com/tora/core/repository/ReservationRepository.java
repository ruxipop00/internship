package com.tora.core.repository;

import com.tora.core.model.Reservation;

public interface ReservationRepository extends IRepository<Reservation, Long> {
}
