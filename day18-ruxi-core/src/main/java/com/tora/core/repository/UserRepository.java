package com.tora.core.repository;

import com.tora.core.model.MyUser;

public interface UserRepository extends IRepository<MyUser, Long> {
}