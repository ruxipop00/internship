package com.tora.core.service;

import com.tora.core.model.MyUser;

import java.util.List;

public interface UserService {
    List<MyUser> findAll();

    MyUser createUser(String name, String email, String password);

    MyUser login(String username, String password) throws Exception;
}
