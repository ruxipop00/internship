package com.tora.core.service;

import com.tora.core.model.Seat;

import java.util.List;

public interface SeatService {
    List<Seat> findAll();
    List<Seat> findAllFromOfficeSpace(Long officeSpaceId);
}
