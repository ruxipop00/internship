package com.tora.core.service;

import com.tora.core.model.OfficeSpace;
import com.tora.core.repository.OfficeSpaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfficeSpaceServiceImpl implements OfficeSpaceService {

    @Autowired
    private OfficeSpaceRepository repository;

    @Override
    public List<OfficeSpace> findAll() {
        return repository.findAll();
    }
}
