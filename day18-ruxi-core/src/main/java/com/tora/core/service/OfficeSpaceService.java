package com.tora.core.service;

import com.tora.core.model.OfficeSpace;

import java.util.List;

public interface OfficeSpaceService {
    List<OfficeSpace> findAll();
}
