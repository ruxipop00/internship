package com.tora.core.service;

import com.tora.core.model.Reservation;

import java.time.LocalDateTime;
import java.util.List;

public interface ReservationService {
    void addReservation(Long userId, Long seatId, LocalDateTime date);
    List<Reservation> findAll();
}
