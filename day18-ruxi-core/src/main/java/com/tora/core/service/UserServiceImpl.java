package com.tora.core.service;

import com.tora.core.model.MyUser;
import com.tora.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Override
    public List<MyUser> findAll() {
        return repository.findAll();
    }

    @Override
    public MyUser createUser(String name, String email, String password) {
        MyUser myUser = MyUser.builder()
                .name(name)
                .email(email)
                .password(password)
                .build();

        repository.save(myUser);
        return myUser;
    }

    @Override
    public MyUser login(String email, String password) throws Exception {
        List<MyUser> myUsers = repository.findAll();
        myUsers.removeIf(user -> !user.getEmail().equals(email) || !user.getPassword().equals(password));

        if (myUsers.isEmpty()) {
            throw new Exception("login failed");
        }
        else {
            return myUsers.get(0);
        }
    }
}
