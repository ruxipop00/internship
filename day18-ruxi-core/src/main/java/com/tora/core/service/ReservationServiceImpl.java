package com.tora.core.service;

import com.tora.core.model.Reservation;
import com.tora.core.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationRepository repository;

    @Override
    public void addReservation(Long userId, Long seatId, LocalDateTime date) {
        Reservation reservation = Reservation.builder()
                .userId(userId)
                .seatId(seatId)
                .date(date)
                .build();

        reservation.setId((long) 0);

        repository.save(reservation);
    }

    @Override
    public List<Reservation> findAll() {
        return repository.findAll();
    }
}
