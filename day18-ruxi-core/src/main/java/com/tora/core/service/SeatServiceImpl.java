package com.tora.core.service;

import com.tora.core.model.Seat;
import com.tora.core.repository.SeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SeatServiceImpl implements SeatService {
    @Autowired
    private SeatRepository repository;

    @Override
    public List<Seat> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Seat> findAllFromOfficeSpace(Long officeSpaceId) {
        List<Seat> seats = repository.findAll();
        seats.removeIf(seat -> !seat.getOfficeSpaceId().equals(officeSpaceId));
        return seats;
    }
}
