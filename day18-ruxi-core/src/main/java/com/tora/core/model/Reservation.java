package com.tora.core.model;

import lombok.*;

import javax.persistence.Entity;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Reservation extends BaseEntity<Long> {
    private Long seatId;
    private Long userId;
    private LocalDateTime date;
}
