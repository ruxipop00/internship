package com.tora.web.controller;

import com.tora.core.model.Seat;
import com.tora.core.service.SeatService;
import com.tora.web.converter.SeatConverter;
import com.tora.web.dto.SeatDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class SeatController {
    @Autowired
    private SeatService seatService;

    @Autowired
    private SeatConverter seatConverter;

    @RequestMapping(value = "/seats", method = RequestMethod.GET)
    public List<SeatDto> getSeats() {
        List<Seat> seats = seatService.findAll();

        return new ArrayList<>(seatConverter.convertModelsToDtos(seats));
    }

    @RequestMapping(value = "/seats/{id}", method = RequestMethod.GET)
    public List<SeatDto> getSeatsFromOffice(@PathVariable Long id){
        List<Seat> seats = seatService.findAllFromOfficeSpace(id);

        return new ArrayList<>(seatConverter.convertModelsToDtos(seats));
    }

}
