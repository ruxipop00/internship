package com.tora.web.controller;

import com.tora.core.model.OfficeSpace;
import com.tora.core.model.Reservation;
import com.tora.core.service.OfficeSpaceService;
import com.tora.core.service.ReservationService;
import com.tora.web.converter.OfficeSpaceConverter;
import com.tora.web.converter.ReservationConverter;
import com.tora.web.dto.OfficeSpaceDto;
import com.tora.web.dto.ReservationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ReservationController {
    @Autowired
    private ReservationService reservationService;

    @Autowired
    private ReservationConverter reservationConverter;

    @RequestMapping(value = "/reserve", method = RequestMethod.GET)
    public List<ReservationDto> getReservations() {
        List<Reservation> reservations = reservationService.findAll();

        return new ArrayList<>(reservationConverter.convertModelsToDtos(reservations));
    }

    @RequestMapping(value = "/reserve", method = RequestMethod.POST)
    ResponseEntity<?> addReservation(@RequestBody ReservationDto reservationDto) {
        Reservation reservation = reservationConverter.convertDtoToModel(reservationDto);
        reservationService.addReservation(
                reservation.getUserId(), reservation.getSeatId(), reservation.getDate()
        );

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
