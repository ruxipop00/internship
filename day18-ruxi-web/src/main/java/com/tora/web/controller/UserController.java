package com.tora.web.controller;

import com.tora.core.model.MyUser;
import com.tora.core.service.UserService;
import com.tora.web.converter.UserConverter;
import com.tora.web.dto.AuthenticationDto;
import com.tora.web.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.security.auth.login.CredentialNotFoundException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserConverter userConverter;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<UserDto> getUsers() {
        List<MyUser> myUsers = userService.findAll();

        return new ArrayList<>(userConverter.convertModelsToDtos(myUsers));
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public UserDto createUser(@RequestBody final UserDto userDto) {
        MyUser myUser = userService.createUser(userDto.getName(), userDto.getEmail(), userDto.getPassword());
        return userConverter.convertModelToDto(myUser);
    }

    @RequestMapping(value = "/auth/login", method = RequestMethod.POST)
    public UserDto login(@RequestBody final AuthenticationDto authenticationDto) throws CredentialNotFoundException {
        MyUser myUser;

        try {
            myUser = userService.login(authenticationDto.getEmail(), authenticationDto.getPassword());
        } catch (Exception e) {
            throw new CredentialNotFoundException("INVALID_CREDENTIALS");
        }

        return userConverter.convertModelToDto(myUser);
    }
}
