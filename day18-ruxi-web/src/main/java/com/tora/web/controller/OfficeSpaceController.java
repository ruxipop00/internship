package com.tora.web.controller;

import com.tora.core.model.OfficeSpace;
import com.tora.core.service.OfficeSpaceService;
import com.tora.web.converter.OfficeSpaceConverter;
import com.tora.web.dto.OfficeSpaceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class OfficeSpaceController {
    @Autowired
    private OfficeSpaceService officeSpaceService;

    @Autowired
    private OfficeSpaceConverter officeSpaceConverter;

    @RequestMapping(value = "/offices", method = RequestMethod.GET)
    public List<OfficeSpaceDto> getOfficeSpaces() {
        List<OfficeSpace> offices = officeSpaceService.findAll();

        return new ArrayList<>(officeSpaceConverter.convertModelsToDtos(offices));
    }
}
