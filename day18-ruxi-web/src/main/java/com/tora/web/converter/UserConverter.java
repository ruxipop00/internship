package com.tora.web.converter;

import com.tora.core.model.MyUser;
import com.tora.web.dto.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserConverter extends AbstractConverterBaseEntityConverter<MyUser, UserDto> {
    @Override
    public MyUser convertDtoToModel(UserDto dto) {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public UserDto convertModelToDto(MyUser myUser) {
        UserDto dto = UserDto.builder()
                .name(myUser.getName())
                .email(myUser.getEmail())
                .password(myUser.getPassword())
                .build();
        dto.setId(myUser.getId());
        return dto;
    }
}