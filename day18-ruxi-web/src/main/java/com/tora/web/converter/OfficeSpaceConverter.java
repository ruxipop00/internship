package com.tora.web.converter;

import com.tora.core.model.OfficeSpace;
import com.tora.web.dto.OfficeSpaceDto;
import org.springframework.stereotype.Component;

@Component
public class OfficeSpaceConverter extends AbstractConverterBaseEntityConverter<OfficeSpace, OfficeSpaceDto> {
    @Override
    public OfficeSpace convertDtoToModel(OfficeSpaceDto dto) {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public OfficeSpaceDto convertModelToDto(OfficeSpace officeSpace) {
        OfficeSpaceDto dto = OfficeSpaceDto.builder()
                .name(officeSpace.getName())
                .location(officeSpace.getLocation())
                .build();
        dto.setId(officeSpace.getId());
        return dto;
    }
}
