package com.tora.web.converter;

import com.tora.core.model.BaseEntity;
import com.tora.web.dto.BaseDto;

public interface ConverterBaseEntity<Model extends BaseEntity<Long>, Dto extends BaseDto>
        extends Converter<Model, Dto> {

}

