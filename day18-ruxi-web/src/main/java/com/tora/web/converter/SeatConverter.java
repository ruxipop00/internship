package com.tora.web.converter;

import com.tora.core.model.Seat;
import com.tora.web.dto.SeatDto;
import org.springframework.stereotype.Component;

@Component
public class SeatConverter extends AbstractConverterBaseEntityConverter<Seat, SeatDto> {
    @Override
    public Seat convertDtoToModel(SeatDto dto) {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public SeatDto convertModelToDto(Seat seat) {
        SeatDto dto = SeatDto.builder()
                .officeSpaceId(seat.getOfficeSpaceId())
                .build();
        dto.setId(seat.getId());
        return dto;
    }
}
