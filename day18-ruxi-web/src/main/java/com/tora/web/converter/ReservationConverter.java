package com.tora.web.converter;

import com.tora.core.model.Reservation;
import com.tora.web.dto.ReservationDto;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class ReservationConverter extends AbstractConverterBaseEntityConverter<Reservation, ReservationDto> {
    @Override
    public Reservation convertDtoToModel(ReservationDto dto) {
        Reservation reservation = Reservation.builder()
                .userId(dto.getUserId())
                .seatId(dto.getSeatId())
                .date(LocalDateTime.parse(dto.getDate(), DateTimeFormatter.ofPattern("MM/dd/yyyy")))
                .build();
        reservation.setId(dto.getId());
        return reservation;
    }

    @Override
    public ReservationDto convertModelToDto(Reservation reservation) {
        ReservationDto dto = ReservationDto.builder()
                .userId(reservation.getUserId())
                .seatId(reservation.getSeatId())
                .date(reservation.getDate().format(DateTimeFormatter.ofPattern("MM/dd/yyyy")))
                .build();
        dto.setId(reservation.getId());
        return dto;
    }
}
