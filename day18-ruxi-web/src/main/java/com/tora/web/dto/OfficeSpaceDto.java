package com.tora.web.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString(callSuper = true)
public class OfficeSpaceDto extends BaseDto {
    private String name;
    private String location;
}