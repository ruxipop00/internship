package com.tora.web.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString(callSuper = true)
public class SeatDto extends BaseDto {
    private Long officeSpaceId;
}
