package com.tora.web.dto;

import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString(callSuper = true)
public class ReservationDto extends BaseDto {
    private Long userId;
    private Long seatId;
    private String date;
}
