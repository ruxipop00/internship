package com.tora.web.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class AuthenticationDto extends BaseDto {
    private String email;
    private String password;

    @Override
    public String toString() {
        return "AuthenticationDto{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}