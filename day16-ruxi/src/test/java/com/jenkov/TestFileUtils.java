package com.jenkov;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TestFileUtils {
    private Path path;
    private String fileName;

    @Before
    public void setup() {
        fileName = "input.txt";
        path = Paths.get(fileName);
    }

    @Test
    public void testFileLines() throws IOException {
        List<String> lines = FileUtils.lines(path).collect(Collectors.toList());
        List<String> testList = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new FileReader(path.toString()));

        String line = reader.readLine();

        while (line != null) {
            testList.add(line);
            line = reader.readLine();
        }
        reader.close();

        assertEquals(lines, testList);
    }

    @Test
    public void testWalk() throws IOException {
        System.out.println(System.getProperty("user.dir"));
        Path testPath = Paths.get("dir1");

        List<String> files = FileUtils.walk(testPath).map(Path::toString).collect(Collectors.toList());
        List<String> testFiles = Files.walk(testPath).map(p -> p.toAbsolutePath().toString()).collect(Collectors.toList());

        assertEquals(files, testFiles);
    }
}
