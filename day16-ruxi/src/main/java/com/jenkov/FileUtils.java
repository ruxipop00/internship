package com.jenkov;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtils {
    public static Stream<String> lines(Path path) throws IOException {
        BufferedReader reader;
        reader = new BufferedReader(new FileReader(path.toString()));
        Stream.Builder<String> builder = Stream.builder();

        String line = reader.readLine();

        // todo: reader.ready() ?
        while (line != null) {
            builder.add(line);

            // read next line
            line = reader.readLine();
        }
        reader.close();

        return builder.build();
    }

    public static Stream<Path> walk(Path path) {
        List<Path> paths = new ArrayList<>();
        walking(path, paths);

        return paths.stream();
    }

    private static void walking(Path path, List<Path> result) {
        File folder = path.toFile();
        File[] files = Objects.requireNonNull(folder.listFiles());
        result.add(Paths.get(folder.getAbsolutePath()));

        for (File file : files) {
            if (file.isFile()) {
                result.add(Paths.get(file.getAbsolutePath()));
            }

            if (file.isDirectory()) {
                walking(file.toPath(), result);
            }
        }
    }

    public static void main(String[] args) {
        Path path = Paths.get("day16/dir1");
        FileUtils.walk(path).forEach(System.out::println);
        System.out.println("one done ---------");
        List<String> files = FileUtils.walk(path).map(p -> p.toAbsolutePath().toString()).collect(Collectors.toList());

        try {
            Files.walk(path).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
