package com.jenkov.collections;

import com.jenkov.InMemoryRepository;
import org.eclipse.collections.impl.list.mutable.FastList;

import java.util.List;

public class FastListBasedRepository<T> implements InMemoryRepository<T> {
    private List<T> fastList;

    public FastListBasedRepository() {
        this.fastList = new FastList<>();
    }

    public FastListBasedRepository(List<T> fastList) {
        this.fastList = new FastList<>();
        this.fastList.addAll(fastList);
    }

    @Override
    public void add(T elem) {
        fastList.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return fastList.contains(elem);
    }

    @Override
    public void remove(T elem) {
        fastList.remove(elem);
    }
}
