package com.jenkov.collections;

import com.jenkov.InMemoryRepository;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;

import java.util.Set;

public class UnifiedSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> unifiedSet;

    public UnifiedSetBasedRepository() {
        this.unifiedSet = new UnifiedSet<>();
    }

    public UnifiedSetBasedRepository(Set<T> unifiedSet) {
        this.unifiedSet = new UnifiedSet<>();
        this.unifiedSet.addAll(unifiedSet);
    }

    @Override
    public void add(T elem) {
        unifiedSet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return unifiedSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        unifiedSet.remove(elem);
    }
}
