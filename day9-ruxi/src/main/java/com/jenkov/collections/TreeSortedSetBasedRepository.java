package com.jenkov.collections;

import com.jenkov.InMemoryRepository;
import org.eclipse.collections.impl.set.sorted.mutable.TreeSortedSet;

import java.util.Set;

public class TreeSortedSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> treeSortedSet;

    public TreeSortedSetBasedRepository() {
        this.treeSortedSet = new TreeSortedSet<>();
    }

    public TreeSortedSetBasedRepository(Set<T> treeSortedSet) {
        this.treeSortedSet = new TreeSortedSet<>();
        this.treeSortedSet.addAll(treeSortedSet);
    }

    @Override
    public void add(T elem) {
        treeSortedSet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return treeSortedSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        treeSortedSet.remove(elem);
    }
}
