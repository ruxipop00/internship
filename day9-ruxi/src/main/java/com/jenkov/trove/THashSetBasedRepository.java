package com.jenkov.trove;

import com.jenkov.InMemoryRepository;
import gnu.trove.set.hash.THashSet;

import java.util.Set;

public class THashSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> tHashSet;

    public THashSetBasedRepository() {
        this.tHashSet = new THashSet<>();
    }

    public THashSetBasedRepository(Set<T> tHashSet) {
        this.tHashSet = new THashSet<>();
        this.tHashSet.addAll(tHashSet);
    }

    @Override
    public void add(T elem) {
        tHashSet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return tHashSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        tHashSet.remove(elem);
    }
}
