/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.jenkov.benchmarks;

import com.jenkov.InMemoryRepository;
import com.jenkov.Order;
import com.jenkov.repository.ArraysListBasedRepository;
import com.jenkov.repository.HashSetBasedRepository;
import com.jenkov.repository.TreeSetBasedRepository;
import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class MyBenchmark {

    @State(Scope.Thread)
    public static class MyState {
        private InMemoryRepository<Order> listRepository;
        private InMemoryRepository<Order> setRepository;
        private InMemoryRepository<Order> treeSetRepository;

        private final int NUMBER_OF_ELEMENTS = 3;
        private Random rand;

        @Setup(Level.Iteration)
        public void doSetup() {
            listRepository = new ArraysListBasedRepository<>();
            setRepository = new HashSetBasedRepository<>();
            treeSetRepository = new TreeSetBasedRepository<>();

            rand = new Random();

            IntStream.rangeClosed(1, NUMBER_OF_ELEMENTS).forEach(i -> listRepository.add(new Order(i,i,i)));
            IntStream.rangeClosed(1, NUMBER_OF_ELEMENTS).forEach(i -> setRepository.add(new Order(i,i,i)));
            IntStream.rangeClosed(1, NUMBER_OF_ELEMENTS).forEach(i -> treeSetRepository.add(new Order(i,i,i)));
        }

        @TearDown(Level.Iteration)
        public void doTearDown() {
            System.out.println("Do TearDown");
        }
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testAddToList(MyState state) {
        int val = state.rand.nextInt(Integer.MAX_VALUE - state.NUMBER_OF_ELEMENTS) + state.NUMBER_OF_ELEMENTS;
        state.listRepository.add(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testRemoveFromList(MyState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.listRepository.remove(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testContainsInList(MyState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.listRepository.contains(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testAddToSet(MyState state) {
        int val = state.rand.nextInt(Integer.MAX_VALUE - state.NUMBER_OF_ELEMENTS) + state.NUMBER_OF_ELEMENTS;
        state.setRepository.add(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testRemoveFromSet(MyState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.setRepository.remove(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testContainsInSet(MyState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.setRepository.contains(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testAddToTreeSet(MyState state) {
        int val = state.rand.nextInt(Integer.MAX_VALUE - state.NUMBER_OF_ELEMENTS) + state.NUMBER_OF_ELEMENTS;
        state.treeSetRepository.add(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testRemoveFromTreeSet(MyState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.treeSetRepository.remove(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testContainsInTreeSet(MyState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.treeSetRepository.contains(new Order(val, val, val));
    }
}
