package com.jenkov.benchmarks;

import com.jenkov.InMemoryRepository;
import com.jenkov.Order;
import com.jenkov.collections.FastListBasedRepository;
import com.jenkov.collections.TreeSortedSetBasedRepository;
import com.jenkov.collections.UnifiedSetBasedRepository;
import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class EclipseCollectionsBenchmark {
    @State(Scope.Thread)
    public static class EclipseCollectionRepositoryState {
        private InMemoryRepository<Order> fastListRepository;
        private InMemoryRepository<Order> unifiedSetRepository;
        private InMemoryRepository<Order> treeSortedSetRepository;


        private final int NUMBER_OF_ELEMENTS = 3;
        private Random rand;

        @Setup(Level.Iteration)
        public void doSetup() {
            fastListRepository = new FastListBasedRepository<>();
            unifiedSetRepository = new UnifiedSetBasedRepository<>();
            treeSortedSetRepository = new TreeSortedSetBasedRepository<>();

            rand = new Random();

            IntStream.rangeClosed(1, NUMBER_OF_ELEMENTS).forEach(i -> fastListRepository.add(new Order(i,i,i)));
            IntStream.rangeClosed(1, NUMBER_OF_ELEMENTS).forEach(i -> unifiedSetRepository.add(new Order(i,i,i)));
            IntStream.rangeClosed(1, NUMBER_OF_ELEMENTS).forEach(i -> treeSortedSetRepository.add(new Order(i,i,i)));
        }

        @TearDown(Level.Iteration)
        public void doTearDown() {
            System.out.println("Do TearDown");
        }
    }


    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testAddToList(EclipseCollectionsBenchmark.EclipseCollectionRepositoryState state) {
        int val = state.rand.nextInt(Integer.MAX_VALUE - state.NUMBER_OF_ELEMENTS) + state.NUMBER_OF_ELEMENTS;
        state.fastListRepository.add(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testRemoveFromList(EclipseCollectionsBenchmark.EclipseCollectionRepositoryState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.fastListRepository.remove(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testContainsInList(EclipseCollectionsBenchmark.EclipseCollectionRepositoryState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.fastListRepository.contains(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testAddToSet(EclipseCollectionsBenchmark.EclipseCollectionRepositoryState state) {
        int val = state.rand.nextInt(Integer.MAX_VALUE - state.NUMBER_OF_ELEMENTS) + state.NUMBER_OF_ELEMENTS;
        state.unifiedSetRepository.add(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testRemoveFromSet(EclipseCollectionsBenchmark.EclipseCollectionRepositoryState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.unifiedSetRepository.remove(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testContainsInSet(EclipseCollectionsBenchmark.EclipseCollectionRepositoryState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.unifiedSetRepository.contains(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testAddToTreeSet(EclipseCollectionsBenchmark.EclipseCollectionRepositoryState state) {
        int val = state.rand.nextInt(Integer.MAX_VALUE - state.NUMBER_OF_ELEMENTS) + state.NUMBER_OF_ELEMENTS;
        state.treeSortedSetRepository.add(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testRemoveFromTreeSet(EclipseCollectionsBenchmark.EclipseCollectionRepositoryState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.treeSortedSetRepository.remove(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testContainsInTreeSet(EclipseCollectionsBenchmark.EclipseCollectionRepositoryState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.treeSortedSetRepository.contains(new Order(val, val, val));
    }
}
