package com.jenkov.benchmarks;

import com.jenkov.InMemoryRepository;
import com.jenkov.Order;
import com.jenkov.fastutil.OpenHashSetBasedRepository;
import com.jenkov.fastutil.RBTreeSetBasedRepository;
import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class FastutilBenchmark {
    @State(Scope.Thread)
    public static class FastutilRepositoryState {
        private InMemoryRepository<Order> openHashSetRepository;
        private InMemoryRepository<Order> rbTreeSetRepository;

        private final int NUMBER_OF_ELEMENTS = 3;
        private Random rand;

        @Setup(Level.Iteration)
        public void doSetup() {
            openHashSetRepository = new OpenHashSetBasedRepository<>();
            rbTreeSetRepository = new RBTreeSetBasedRepository<>();

            rand = new Random();

            IntStream.rangeClosed(1, NUMBER_OF_ELEMENTS).forEach(i -> openHashSetRepository.add(new Order(i,i,i)));
            IntStream.rangeClosed(1, NUMBER_OF_ELEMENTS).forEach(i -> rbTreeSetRepository.add(new Order(i,i,i)));
        }

        @TearDown(Level.Iteration)
        public void doTearDown() {
            System.out.println("Do TearDown");
        }
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testAddToSet(FastutilBenchmark.FastutilRepositoryState state) {
        int val = state.rand.nextInt(Integer.MAX_VALUE - state.NUMBER_OF_ELEMENTS) + state.NUMBER_OF_ELEMENTS;
        state.openHashSetRepository.add(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testRemoveFromSet(FastutilBenchmark.FastutilRepositoryState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.openHashSetRepository.remove(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testContainsInSet(FastutilBenchmark.FastutilRepositoryState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.openHashSetRepository.contains(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testAddToTreeSet(FastutilBenchmark.FastutilRepositoryState state) {
        int val = state.rand.nextInt(Integer.MAX_VALUE - state.NUMBER_OF_ELEMENTS) + state.NUMBER_OF_ELEMENTS;
        state.rbTreeSetRepository.add(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testRemoveFromTreeSet(FastutilBenchmark.FastutilRepositoryState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.rbTreeSetRepository.remove(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testContainsInTreeSet(FastutilBenchmark.FastutilRepositoryState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.rbTreeSetRepository.contains(new Order(val, val, val));
    }
}
