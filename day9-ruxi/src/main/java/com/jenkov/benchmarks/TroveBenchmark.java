package com.jenkov.benchmarks;

import com.jenkov.InMemoryRepository;
import com.jenkov.Order;
import com.jenkov.trove.THashSetBasedRepository;
import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class TroveBenchmark {
    @State(Scope.Thread)
    public static class TroveRepositoryState {
        private InMemoryRepository<Order> tHashSetRepository;
        private final int NUMBER_OF_ELEMENTS = 3;
        private Random rand;

        @Setup(Level.Iteration)
        public void doSetup() {
            tHashSetRepository = new THashSetBasedRepository<>();
            rand = new Random();
            IntStream.rangeClosed(1, NUMBER_OF_ELEMENTS).forEach(i -> tHashSetRepository.add(new Order(i,i,i)));
        }

        @TearDown(Level.Iteration)
        public void doTearDown() {
            System.out.println("Do TearDown");
        }
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testAddToSet(TroveBenchmark.TroveRepositoryState state) {
        int val = state.rand.nextInt(Integer.MAX_VALUE - state.NUMBER_OF_ELEMENTS) + state.NUMBER_OF_ELEMENTS;
        state.tHashSetRepository.add(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testRemoveFromSet(TroveBenchmark.TroveRepositoryState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.tHashSetRepository.remove(new Order(val, val, val));
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 3, time = 3)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Measurement(iterations = 3, time = 2)
    public void testContainsInSet(TroveBenchmark.TroveRepositoryState state) {
        int val = state.rand.nextInt(state.NUMBER_OF_ELEMENTS);
        state.tHashSetRepository.contains(new Order(val, val, val));
    }
}
