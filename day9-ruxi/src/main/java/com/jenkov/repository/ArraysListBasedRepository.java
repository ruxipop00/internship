package com.jenkov.repository;

import com.jenkov.InMemoryRepository;

import java.util.ArrayList;
import java.util.List;

public class ArraysListBasedRepository<T> implements InMemoryRepository<T> {
    private List<T> elems;

    public ArraysListBasedRepository() {
        this.elems = new ArrayList<>();
    }

    public ArraysListBasedRepository(List<T> elems) {
        this.elems = new ArrayList<>();
        this.elems.addAll(elems);
    }

    @Override
    public void add(T elem) {
        elems.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return elems.contains(elem);
    }

    @Override
    public void remove(T elem) {
        elems.remove(elem);
    }
}
