package com.jenkov.repository;

import com.jenkov.InMemoryRepository;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> elems;

    public HashSetBasedRepository() {
        this.elems = new HashSet<>();
    }

    public HashSetBasedRepository(Set<T> elems) {
        this.elems = new HashSet<>();
        this.elems.addAll(elems);
    }

    @Override
    public void add(T elem) {
        elems.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return elems.contains(elem);
    }

    @Override
    public void remove(T elem) {
        elems.remove(elem);
    }
}
