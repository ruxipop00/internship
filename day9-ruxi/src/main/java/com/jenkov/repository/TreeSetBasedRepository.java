package com.jenkov.repository;

import com.jenkov.InMemoryRepository;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> elems;

    public TreeSetBasedRepository() {
        this.elems = new TreeSet<>();
    }

    public TreeSetBasedRepository(Set<T> elems) {
        this.elems = new TreeSet<>();
        this.elems.addAll(elems);
    }

    @Override
    public void add(T elem) {
        elems.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return elems.contains(elem);
    }

    @Override
    public void remove(T elem) {
        elems.remove(elem);
    }
}
