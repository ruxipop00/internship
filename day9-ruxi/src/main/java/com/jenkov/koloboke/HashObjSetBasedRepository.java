package com.jenkov.koloboke;

import com.jenkov.InMemoryRepository;
import com.koloboke.collect.set.hash.HashObjSets;

import java.util.Set;

public class HashObjSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> hashObjSet;

    public HashObjSetBasedRepository() {
        this.hashObjSet = HashObjSets.newMutableSet();
    }

    public HashObjSetBasedRepository(Set<T> hashObjSet) {
        this.hashObjSet = HashObjSets.newMutableSet();
        this.hashObjSet.addAll(hashObjSet);
    }

    @Override
    public void add(T elem) {
        hashObjSet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return hashObjSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        hashObjSet.remove(elem);
    }
}
