package com.jenkov.fastutil;

import com.jenkov.InMemoryRepository;
import it.unimi.dsi.fastutil.objects.ObjectRBTreeSet;

import java.util.Set;

public class RBTreeSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> rbTreeSet;

    public RBTreeSetBasedRepository() {
        this.rbTreeSet = new ObjectRBTreeSet<>();
    }

    public RBTreeSetBasedRepository(Set<T> rbTreeSet) {
        this.rbTreeSet = new ObjectRBTreeSet<>();
        this.rbTreeSet.addAll(rbTreeSet);
    }

    @Override
    public void add(T elem) {
        rbTreeSet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return rbTreeSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        rbTreeSet.remove(elem);
    }
}
