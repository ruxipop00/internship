package com.jenkov.fastutil;

import com.jenkov.InMemoryRepository;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;

import java.util.Set;

public class OpenHashSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> openHashSet;

    public OpenHashSetBasedRepository() {
        this.openHashSet = new ObjectOpenHashSet<>();
    }

    public OpenHashSetBasedRepository(Set<T> openHashSet) {
        this.openHashSet = new ObjectOpenHashSet<>();
        this.openHashSet.addAll(openHashSet);
    }

    @Override
    public void add(T elem) {
        openHashSet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return openHashSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        openHashSet.remove(elem);
    }
}
