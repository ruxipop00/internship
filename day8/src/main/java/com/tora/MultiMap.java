package com.tora;

import java.util.HashMap;
import java.util.Map;

public class MultiMap<K1, K2, V> {
    private Map<K1,Map<K2,V>> multiMap;

    public MultiMap() {
        this.multiMap = new HashMap<>();
    }

    public V put(K1 k1, K2 k2, V v) {

        if (multiMap.containsKey(k1)) {
            Map<K2,V> secondMap = multiMap.get(k1);
            return secondMap.put(k2, v);
        }

        Map<K2,V> secondMap = new HashMap<>();
        multiMap.put(k1, secondMap);
        return secondMap.put(k2, v);
    }

    public V get(K1 k1, K2 k2) {
        if(!multiMap.containsKey(k1)) {
            return null;
        }

        Map<K2,V> m2 = multiMap.get(k1);
        return m2.get(k2);
    }

    public boolean containsKey(K1 k1, K2 k2) {
        if (!multiMap.containsKey(k1)) {
            return false;
        }

        Map<K2,V> m2 = multiMap.get(k1);
        return m2.containsKey(k2);
    }

    public V remove(K1 k1, K2 k2) {
        if(!multiMap.containsKey(k1)) {
            return null;
        }

        Map<K2,V> m2 = multiMap.get(k1);

        if (m2.size() == 1) {
            V removedElem = m2.remove(k2);
            multiMap.remove(k1);
            return removedElem;
        }

        return m2.remove(k2);
    }

    public int size() {
        return multiMap.values().stream()
                .mapToInt(Map::size)
                .sum();
    }

    @Override
    public String toString() {
        return "MultiMap{" +
                "multiMap=" + multiMap +
                '}';
    }
}
