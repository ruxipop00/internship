package com.tora;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestMultiMap {
    private MultiMap<Integer, Integer, String> multiMap;

    @Before
    public void setup() {
        multiMap = new MultiMap<>();
    }

    @Test
    public void testSize() {
        multiMap.put(1,1,"value1");
        multiMap.put(1,1,"value2");
        multiMap.put(2,1,"value2");
        multiMap.put(2,3,"value2");
        multiMap.put(2,4,"value2");

        assertThat(multiMap.size(), is(4));
    }

    @Test
    public void testRemove() {
        multiMap.put(1,1,"value1");
        multiMap.put(2,1,"value2");
        multiMap.put(3,1,"value3");
        multiMap.put(2,1,"value2");
        multiMap.remove(2,1);
        multiMap.remove(2,1);

        assertThat(multiMap.size(), is(2));
    }

    @Test
    public void testGet() {
        multiMap.put(1, 2, "value1");
        multiMap.put(1, 5, "value2");
        multiMap.put(33, 2, "value3");

        assertThat(multiMap.get(1, 2), is("value1"));
        assertThat(multiMap.get(1, 5), is("value2"));
        assertThat(multiMap.get(33, 2), is("value3"));
    }

    @Test
    public void testContainsKey() {
        multiMap.put(1, 2, "value1");
        multiMap.put(1, 5, "value2");
        multiMap.put(33, 2, "value3");

        assertThat(multiMap.containsKey(1, 2), is(true));
        assertThat(multiMap.containsKey(1, 1), is(false));
        assertThat(multiMap.containsKey(33, 2), is(true));
    }
}
