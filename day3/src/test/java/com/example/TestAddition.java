package com.example;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestAddition {

    @Test
    public void correctResult() {
        int x  = 5 ;
        int y = 3;

        assertThat(Addition.add(x,y), is(8));
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForNegativeNumbers() {
        int x  = -111 ;
        int y = 2;

        Addition.add(x,y);
    }

    @Test(expected = IllegalArgumentException.class)
    public void maximumValueForParameters() {
        int x  = Integer.MAX_VALUE / 2 + 1;
        int y = 2;

        Addition.add(x,y);
    }
}
