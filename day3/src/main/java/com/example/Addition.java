package com.example;

public class Addition {
    public static int add(int a, int b) {
        int lowLimit = 0;
        int highLimit = Integer.MAX_VALUE / 2;

        if (a<lowLimit || b<lowLimit) {
            throw new IllegalArgumentException("Numbers have to be positive!");
        }
        if (a>highLimit || b>highLimit) {
            throw new IllegalArgumentException("Numbers are too large!");
        }

        return a+b;
    }
}
