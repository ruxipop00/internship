package com.tora;

import java.util.*;

public class FlattenedIterator implements Iterator<Integer> {
    private LinkedList<Iterator<Integer>> iterators;

    public FlattenedIterator(List<Iterator<Integer>> subiterators) {
        iterators = new LinkedList<>();
        for (Iterator<Integer> it : subiterators) {
            iterators.addLast(it);
        }
    }

    @Override
    public boolean hasNext() {
        return iterators.size() != 0;
    }

    @Override
    public Integer next() {
//        if (!hasNext()) {
//            //throw exception
//        }

        Iterator<Integer> it = iterators.pop();
        Integer val = it.next();

        if(it.hasNext()) {
            iterators.add(it);
        }
        return val;
    }

    public static void main(String[] args) {
        Integer[] a1 = new Integer[] { 1, 2, 3 };
        Integer[] a2 = new Integer[] { 4, 5 };
        Integer[] a3 = new Integer[] { 6, 7, 8 };

        List<Integer> l1 = Arrays.asList(a1);
        List<Integer> l2 = Arrays.asList(a2);
        List<Integer> l3 = Arrays.asList(a3);

        Iterator<Integer> it1 = l1.iterator();
        Iterator<Integer> it2 = l2.iterator();
        Iterator<Integer> it3 = l3.iterator();

        List<Iterator<Integer>> itList = new ArrayList<>();
        itList.add(it1);
        itList.add(it2);
        itList.add(it3);

        Iterator<Integer> flattenedIterator = new FlattenedIterator(itList);

        while (flattenedIterator.hasNext())
            System.out.print(flattenedIterator.next() + " ");
    }
}

